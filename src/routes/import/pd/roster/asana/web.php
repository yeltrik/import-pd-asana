<?php

use Illuminate\Support\Facades\Route;
use Yeltrik\ImportPDAsana\app\http\controllers\ImportPDRosterAsanaController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('import/pd/rosters/asana/create',
    [ImportPDRosterAsanaController::class, 'create'],)
    ->name('imports.pds.rosters.asana.create');

Route::post('import/pd/rosters/asana',
    [ImportPDRosterAsanaController::class, 'store'],)
    ->name('imports.pds.rosters.asana.store');
