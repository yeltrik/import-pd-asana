<?php


namespace Yeltrik\ImportPDAsana\app;


use Yeltrik\ImportPDAsana\app\models\SessionAsanaTask;
use Yeltrik\PdPSR\app\models\Session;

class AsanaPDSessionUpdater extends Abstract_AsanaPDTaskImporter
{

    /**
     *
     */
    public function process()
    {
        if (static::taskExists($this->task())) {
            $session = $this->getSessionFromTask($this->task());
            if ($session instanceof Session) {
                $name = $this->task()['name'];
                $session->title = $name;
                $session->save();
            } else {
                dd([
                    'Task does not exist',
                    $this->task()
                ]);
            }
        }
    }

    /**
     * @param array $task
     * @return bool
     */
    public static function taskExists(array $task)
    {
        $gid = $task['gid'];
        $sessionAsanaTask = SessionAsanaTask::query()
            ->where('asana_gid', '=', $gid)
            ->first();

        return ($sessionAsanaTask instanceof SessionAsanaTask);
    }

}
