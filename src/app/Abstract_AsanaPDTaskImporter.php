<?php


namespace Yeltrik\ImportPDAsana\app;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\ImportAsana\app\import\Abstract_ImportAsanaJsonTask;
use Yeltrik\ImportPDAsana\app\models\SessionAsanaTask;
use Yeltrik\PdPSR\app\models\Session;
use Yeltrik\UniTrm\app\models\Term;
use Yeltrik\UniTrm\app\models\Year;

abstract class Abstract_AsanaPDTaskImporter extends Abstract_ImportAsanaJsonTask
{

    /**
     * @param array $task
     * @return HigherOrderBuilderProxy|mixed|Session|null
     */
    protected function getSessionFromTask(array $task)
    {
        $gid = $task['gid'];
        $sessionAsanaTask = SessionAsanaTask::query()
            ->where('asana_gid', '=', $gid)
            ->first();
        if ($sessionAsanaTask instanceof SessionAsanaTask) {
            $session = $sessionAsanaTask->session;
            return $session;
        } else {
            return NULL;
        }
    }

    /**
     * @return HigherOrderBuilderProxy|mixed|Session|null
     */
    protected function getTermFromTask()
    {
        $yearStr = NULL;
        $termStr = NULL;
        foreach ($this->task()['custom_fields'] as $customField) {
            switch ($customField['name']) {
                case "Term":
                    if (array_key_exists('enum_value', $customField)) {
                        $enumValue = $customField['enum_value'];
                        if (is_array($enumValue)) {
                            $termStr = $enumValue['name'];
                            $year = $this->getYearFromTask();
                            if ($termStr != NULL) {
                                $term = Term::query()
                                    ->where('name', '=', $termStr);
                                if ($term->exists() === TRUE) {
                                    $term = $term->first();
                                } else {
                                    $term = new Term();
                                    $term->year()->associate($year);
                                    $term->name = $termStr;
                                    $term->abbr = substr($termStr, 0, 2);
                                    $term->save();
                                }
                                return $term;
                            } else {
                                return NULL;
                            }
                        } else {
                            return NULL;
                        }
                    } else {
                        return NULL;
                    }
                    break;
            }
        }
        return NULL;
    }

    /**
     * @return Builder|Model|object|Year|null
     */
    protected function getYearFromTask()
    {
        $yearStr = NULL;
        foreach ($this->task()['custom_fields'] as $customField) {
            switch ($customField['name']) {
                case "Year":
                    $yearStr = $customField['number_value'];
                    if ($yearStr != NULL) {
                        $year = Year::query()
                            ->where('name', '=', $yearStr);
                        if ($year->exists() === TRUE) {
                            $year = $year->first();
                        } else {
                            $year = new Year();
                            $year->name = $yearStr;
                            $year->abbr = substr($yearStr, -2);
                            $year->save();
                        }
                        return $year;
                    } else {
                        return NULL;
                    }
                    break;
            }
        }


    }

}
