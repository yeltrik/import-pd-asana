<?php


namespace Yeltrik\ImportPDAsana\app;


use Illuminate\Database\Eloquent\HigherOrderBuilderProxy;
use Illuminate\Http\Request;
use Yeltrik\ImportAsana\app\import\Abstract_ImportAsanaJson;
use Yeltrik\ImportPDAsana\app\models\SessionAsanaTask;
use Yeltrik\PdPSR\app\models\Session;

abstract class Abstract_AsanaPDImporter extends Abstract_ImportAsanaJson
{

    /**
     * @param array $task
     * @return HigherOrderBuilderProxy|mixed|Session|null
     */
    protected function getSessionFromTask(array $task)
    {
        $gid = $task['gid'];
        $sessionAsanaTask = SessionAsanaTask::query()
            ->where('asana_gid', '=', $gid)
            ->first();
        if ($sessionAsanaTask instanceof SessionAsanaTask) {
            $session = $sessionAsanaTask->session;
            return $session;
        } else {
            return NULL;
        }
    }

}
