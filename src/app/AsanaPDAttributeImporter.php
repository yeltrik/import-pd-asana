<?php


namespace Yeltrik\ImportPDAsana\app;

use Yeltrik\ImportPDAsana\app\models\PDPSRUniTrm;
use Yeltrik\PdPSR\app\models\Session;
use Yeltrik\PdPSR\app\models\SessionTag;
use Yeltrik\UniTrm\app\models\Term;

class AsanaPDAttributeImporter extends Abstract_AsanaPDTaskImporter
{

    /**
     *
     */
    public function process()
    {
        if ($this->request()->import_tags) {
            $this->processTaskTags($this->task());
        }

        if ($this->request()->import_start_on_as_start_date) {
            $session = $this->getSessionFromTask($this->task());
            $dueOn = $this->task()['start_on'];
            $session->start = $dueOn;
            $session->save();
        }

        if ($this->request()->import_due_on_as_end_date) {
            $session = $this->getSessionFromTask($this->task());
            $session = $this->getSessionFromTask($this->task());
            $dueOn = $this->task()['due_on'];
            $session->end = $dueOn;
            $session->save();
        }

        if ($this->request()->import_uni_trm) {
            $term = $this->getTermFromTask();
            if ( $term instanceof Term ) {
                $pdpsrUniTrmQuery = PDPSRUniTrm::query()
                    ->where('session_id', '=', $session->id);
                if ( $pdpsrUniTrmQuery->exists() === TRUE ) {
                    $pdpsrUniTrm = $pdpsrUniTrmQuery->first();
                } else {
                    $pdpsrUniTrm = new PDPSRUniTrm();
                    $pdpsrUniTrm->session()->associate($session);
                }
                $pdpsrUniTrm->term()->associate($term);
                $pdpsrUniTrm->save();
            }
        }
    }

    /**
     * @param array $task
     */
    private function processTaskTags(array $task)
    {
        $session = $this->getSessionFromTask($task);
        if ($session instanceof Session) {
            $sessionTags = $session->tags;
            foreach ($sessionTags as $sessionTag) {
                $sessionTag->delete();
            }
            // Remove Existing Tags
            //dd($sessionTags);
            // Add Tags
            $taskTags = $task['tags'];
            //dd($taskTags);
            foreach ($taskTags as $taskTag) {
                $name = $taskTag['name'];
                $sessionTag = new SessionTag();
                $sessionTag->session()->associate($session);
                $sessionTag->tag_title = $name;
                $sessionTag->save();
            }

        } else {
            dd([
                'Task does not exist',
                $task
            ]);
        }
    }

}
