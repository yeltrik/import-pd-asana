<?php

namespace Yeltrik\ImportPDAsana\app\models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Yeltrik\PdPSR\app\models\Session;
use Yeltrik\UniTrm\app\models\Term;

/**
 * Class PDPSRUniTrm
 *
 * @property int id
 * @property int session_id
 * @property int term_id
 *
 * @property Session $session
 * @property Term $term
 *
 * @package Yeltrik\ImportPDAsana\app\models
 */
class PDPSRUniTrm extends Model
{
    use HasFactory;

    protected $connection = 'import_pd_asana';
    public $table = 'pd-psr-uni-trm';

    /**
     * @return BelongsTo
     */
    public function session()
    {
        return $this->belongsTo(Session::class);
    }

    /**
     * @return BelongsTo
     */
    public function term()
    {
        return $this->belongsTo(Term::class);
    }

}
