<?php

namespace Yeltrik\ImportPDAsana\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Yeltrik\PdPSR\app\models\Session;

/**
 * Class SessionAsanaTask
 *
 * @property int session_id
 * @property string asana_gid
 * @property string last_scanned_attachments
 *
 * @property string href
 *
 * @property Session session
 *
 * @package Yeltrik\ImportPDAsana\app\models
 */
class SessionAsanaTask extends Model
{
    use HasFactory;

    protected $connection = 'import_pd_asana';
    public $table = 'session_asana_task';

    /**
     * @return string
     */
    public function getHrefAttribute()
    {
        if (env('ASANA_PD_GID') != "" ) {
            return 'https://app.asana.com/0/' . env('ASANA_PD_GID') . '/' . $this->asana_gid . '/f';
        } else {
            return "";
        }
    }

    /**
     * @return BelongsTo
     */
    public function session()
    {
        return $this->belongsTo(Session::class);
    }

}
