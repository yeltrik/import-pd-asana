<?php

namespace Yeltrik\ImportPDAsana\app\models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Yeltrik\PdPSR\app\models\Session;

/**
 * Class SessionAsanaAttachment
 *
 * @property int id
 * @property int session_id
 * @property string asana_gid
 * @property string name
 * @property string date_imported_roster
 * @property string download_path
 * @property string row_count
 *
 * @property Session session
 *
 * @package App\Models
 */
class SessionAsanaAttachment extends Model
{
    use HasFactory;

    protected $connection = 'import_pd_asana';
    public $table = 'session_asana_attachments';

    /**
     * @return BelongsTo
     */
    public function session()
    {
        return $this->belongsTo(Session::class);
    }

}
