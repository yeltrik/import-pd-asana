<?php


namespace Yeltrik\ImportPDAsana\app;


use Yeltrik\ImportPDAsana\app\models\SessionAsanaTask;
use Yeltrik\PdPSR\app\models\Session;

class AsanaPDSessionCreator extends Abstract_AsanaPDTaskImporter
{

    /**
     *
     */
    public function process()
    {
        if (static::taskIsNew($this->task())) {
            $gid = $this->task()['gid'];

            $name = $this->task()['name'];
            $session = new Session();
            $session->title = $name;
            $session->save();

            $sessionAsanaTask = new SessionAsanaTask();
            $sessionAsanaTask->session()->associate($session);
            $sessionAsanaTask->asana_gid = $gid;
            $sessionAsanaTask->save();
        }
    }

    public static function taskIsNew(array $task)
    {
        return !AsanaPDSessionUpdater::taskExists($task);
    }

}
