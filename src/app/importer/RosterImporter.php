<?php


namespace Yeltrik\ImportPDAsana\app\importer;


use Illuminate\Database\Eloquent\Builder;
use Yeltrik\PdPSR\app\models\Roster;
use Yeltrik\PdPSR\app\models\Session;
use Yeltrik\Profile\app\models\Profile;

class RosterImporter
{

    /**
     * @param Session $session
     * @param Profile $profile
     * @param bool $attended
     * @return Roster
     */
    public static function process(Session $session, Profile $profile, bool $attended): Roster
    {
        if (static::rosterExists($session, $profile)) {
            $roster = static::rosterQuery($session, $profile)
                ->first();
        } else {
            $roster = new Roster();
            $roster->session()
                ->associate($session);
            $roster->profile()
                ->associate($profile);
        }
        $roster->attended = $attended;
        $roster->save();
        return $roster;
    }

    /**
     * @param Session $session
     * @param Profile $profile
     * @return bool
     */
    public static function rosterExists(Session $session, Profile $profile): bool
    {
        return static::rosterQuery($session, $profile)
            ->exists();
    }

    /**
     * @param Session $session
     * @param Profile $profile
     * @return Builder
     */
    public static function rosterQuery(Session $session, Profile $profile): Builder
    {
        return Roster::query()
            ->where('session_id', '=', $session->id)
            ->where('profile_id', '=', $profile->id);
    }

}
