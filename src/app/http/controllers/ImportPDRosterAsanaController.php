<?php

namespace Yeltrik\ImportPDAsana\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yeltrik\ImportPDAsana\app\import\AsanaPDRosterImporter;

/**
 * Class ImportPDAsanaController
 * @package Yeltrik\ImportPDAsana\app\http\controllers
 */
class ImportPDRosterAsanaController extends Controller
{

    /**
     * ImportPDAsanaController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('importPDAsana');
        return view('importPDAsana::import.pd.roster.asana.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws AuthorizationException
     */
    public function store(Request $request)
    {
        $this->authorize('importPDAsana');
        $request->validate([
            'mode' => 'required',
        ]);

        switch($request->mode) {
            case 'scan_new_attachment':
            case 'scan_removed_attachment':
            case 'download_attachment':
            case "scan_attachment_roster":
                AsanaPDRosterImporter::validate($request);
                $asanaPDRosterImporter = new AsanaPDRosterImporter($request);
                return $asanaPDRosterImporter->process($request->mode);
                break;
            default:
                dd('Unsupported Mode: ' . $request->mode);
        }
    }
}
