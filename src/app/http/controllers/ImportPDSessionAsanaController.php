<?php

namespace Yeltrik\ImportPDAsana\app\http\controllers;

use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Yeltrik\ImportPDAsana\app\import\AsanaPDSessionJsonImporter;

/**
 * Class ImportPDAsanaController
 * @package Yeltrik\ImportPDAsana\app\http\controllers
 */
class ImportPDSessionAsanaController extends Controller
{

    /**
     * ImportPDAsanaController constructor.
     */
    public function __construct()
    {
        $this->middleware(['web', 'auth']);
    }

    /**
     * @return Application|Factory|View
     * @throws AuthorizationException
     */
    public function create()
    {
        $this->authorize('importPDAsana');
        return view('importPDAsana::import.pd.session.asana.create');
    }

    /**
     * @param Request $request
     * @return RedirectResponse
     * @throws AuthorizationException
     * @throws FileNotFoundException
     */
    public function store(Request $request)
    {
        $this->authorize('importPDAsana');
        $request->validate([
            'import_method' => 'required',
        ]);

        switch($request->import_method) {
            case "export_json":
                AsanaPDSessionJsonImporter::validate($request);
                $importer = new AsanaPDSessionJsonImporter($request);
                return $importer->process();
            default:
                dd('Unsupported Import Method: ' . $request->import_method);
        }
    }
}
