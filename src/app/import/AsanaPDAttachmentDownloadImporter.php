<?php


namespace Yeltrik\ImportPDAsana\app\import;


use Asana\Client;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Yeltrik\ImportPDAsana\app\models\SessionAsanaAttachment;

class AsanaPDAttachmentDownloadImporter
{

    private static array $supportedExtensions = [
        'csv'
    ];

    private string $downloadPath = "";
    private SessionAsanaAttachment $sessionAsanaAttachment;

    /**
     * AsanaPDAttachmentDownloadImporter constructor.
     * @param SessionAsanaAttachment $sessionAsanaAttachment
     */
    public function __construct(SessionAsanaAttachment $sessionAsanaAttachment)
    {
        $this->sessionAsanaAttachment = $sessionAsanaAttachment;
    }

    /**
     * @return string
     */
    public function getDownloadPath()
    {
        return $this->downloadPath;
    }

    /**
     *
     */
    public function process()
    {
        $sessionAsanaAttachment = $this->sessionAsanaAttachment();
        $pathParts = pathinfo($sessionAsanaAttachment->name);
        $extension = $pathParts['extension'];
        if ( in_array($extension, static::$supportedExtensions) ) {
            $gid = $sessionAsanaAttachment->asana_gid;
            $asanaClient = Client::accessToken(env('ASANA_PERSONAL_ACCESS_TOKEN'));
            $attachment = $asanaClient->attachments->getAttachment(
                $gid,
                [],
                ['opt_fields' => 'name,download_url,created_at']
            );

            $downloadUrl = $attachment->download_url;

            // TODO: Query file_get_contents to file
            // TODO: Query create public storage file

            $name = $attachment->name;
            $fileName = time() . '_' . $gid . '_' . $name;
            $filePath = "asana/task/attachment/$fileName";
            $contents = file_get_contents($downloadUrl);
            Storage::disk('public')->put($filePath, $contents);

            $sessionAsanaAttachment->name = $name;
            $sessionAsanaAttachment->download_path = $filePath;
            $sessionAsanaAttachment->save();
        }
    }

    /**
     * @return Builder|Model|object|null
     */
    private function sessionAsanaAttachment()
    {
        return $this->sessionAsanaAttachment;
    }

}
