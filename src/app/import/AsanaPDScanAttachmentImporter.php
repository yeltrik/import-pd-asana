<?php


namespace Yeltrik\ImportPDAsana\app\import;


use Asana\Client;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\ImportPDAsana\app\models\SessionAsanaAttachment;
use Yeltrik\ImportPDAsana\app\models\SessionAsanaTask;

class AsanaPDScanAttachmentImporter extends Abstract_AsanaPDScanAttachment
{

    /**
     * @param object $attachment
     * @return bool
     */
    private function attachmentExists(object $attachment)
    {
        $gid = $attachment->gid;
        $sessionAsanaAttachment = SessionAsanaAttachment::query()
            ->where('asana_gid', '=', $gid)
            ->first();
        return ($sessionAsanaAttachment instanceof SessionAsanaAttachment);
    }

    /**
     * @param object $attachment
     * @return bool
     */
    private function attachmentIsNew(object $attachment)
    {
        return !$this->attachmentExists($attachment);
    }

    /**
     *
     */
    public function process()
    {
        $sessionAsanaTask = $this->sessionAsanaTask();
        if ( $sessionAsanaTask instanceof SessionAsanaTask ) {
            $gid = $sessionAsanaTask->asana_gid;
            $asanaClient = Client::accessToken(env('ASANA_PERSONAL_ACCESS_TOKEN'));
            $attachments = $asanaClient->attachments->getAttachmentsForTask(
                $gid,
                [],
                ['opt_fields' => 'name,download_url,created_at']
            );
            foreach ($attachments as $attachment) {
                if ($this->attachmentIsNew($attachment)) {
                    $this->processNewAttachment($attachment);
                }
            }
        }
    }

    /**
     * @param $attachment
     */
    public function processNewAttachment($attachment)
    {
        $gid = $attachment->gid;
        $name = $attachment->name;
        $sessionAsanaAttachment = new SessionAsanaAttachment();
        $sessionAsanaAttachment->session()->associate($this->session());
        $sessionAsanaAttachment->asana_gid = $gid;
        $sessionAsanaAttachment->name = $name;
        $sessionAsanaAttachment->save();
    }

}
