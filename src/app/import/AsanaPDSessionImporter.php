<?php

namespace Yeltrik\ImportPDAsana\app\import;

use Illuminate\Http\Request;
use Yeltrik\ImportPDAsana\app\AsanaPDAttributeImporter;
use Yeltrik\ImportPDAsana\app\AsanaPDSessionCreator;
use Yeltrik\ImportPDAsana\app\AsanaPDSessionUpdater;

/**
 * Class AsanaPDSessionImporter
 * @package Yeltrik\ImportPDAsana\app\import
 */
class AsanaPDSessionImporter
{

    private bool $import;
    private Request $request;
    private array $tasks;
    private bool $update;

    /**
     * AsanaPDSessionImporter constructor.
     * @param Request $request
     * @param array $tasks
     * @param bool $import
     * @param bool $update
     */
    public function __construct(Request $request, array $tasks, bool $import = FALSE, bool $update = FALSE)
    {
        $this->request = $request;
        $this->tasks = $tasks;
        $this->import = $import;
        $this->update = $update;
    }

    /**
     *
     */
    public function process()
    {
        foreach ($this->tasks() as $key => $task) {
            $this->processTask($task);
        }
    }

    /**
     * @param array $task
     */
    private function processAttributes(array $task)
    {
        $asanaPDAttributeImporter = new AsanaPDAttributeImporter($this->request(), $task);
        $asanaPDAttributeImporter->process();
    }

    /**
     * @param array $task
     */
    private function processTask(array $task)
    {
        if (
            $this->shouldImport() &&
            AsanaPDSessionCreator::taskIsNew($task)
        ) {
            $asanaPDSessionCreator = new AsanaPDSessionCreator($this->request(), $task);
            $asanaPDSessionCreator->process();
            $this->processAttributes($task);
        }

        if (
            $this->shouldUpdate() &&
            AsanaPDSessionUpdater::taskExists($task)
        ) {
            $asanaPDSessionUpdater = new AsanaPDSessionUpdater($this->request(), $task);
            $asanaPDSessionUpdater->process();
            $this->processAttributes($task);
        }
    }

    /**
     * @return Request
     */
    private function request()
    {
        return $this->request;
    }

    /**
     * @return bool
     */
    private function shouldImport()
    {
        return $this->import;
    }

    /**
     * @return bool
     */
    private function shouldUpdate()
    {
        return $this->update;
    }

    /**
     * @return array
     */
    public function tasks()
    {
        return $this->tasks;
    }

}
