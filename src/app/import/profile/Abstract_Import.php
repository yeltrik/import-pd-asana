<?php


namespace Yeltrik\ImportPDAsana\app\import\profile;


use Yeltrik\Profile\app\models\Profile;

abstract class Abstract_Import
{

    private array $data;
    private ?Profile $profile = NULL;

    /**
     * @param array $data
     */
    public function __construct(array $data, Profile $profile = NULL)
    {
        $this->data = $data;
        $this->profile = $profile;
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        if ($this->__isset($name)) {
            return $this->data()[$name];
        } else {
            return NULL;
        }
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __isset($name)
    {
        return array_key_exists($name, $this->data());
    }

    /**
     * @return Profile
     */
    public function createProfile()
    {
        $profile = new Profile();
        $profile->save();
        return $profile;
    }

    /**
     * @return array
     */
    protected function data()
    {
        return $this->data;
    }

    /**
     * @return Profile
     */
    public function profile()
    {
        if ($this->profile instanceof Profile === FALSE) {
            $this->profile = $this->createProfile();
        }
        return $this->profile;
        // TODO: Can we Lookup a profile instead of Creating?
    }

}
