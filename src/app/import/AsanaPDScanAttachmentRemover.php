<?php


namespace Yeltrik\ImportPDAsana\app\import;


use Asana\Client;
use Illuminate\Support\Facades\Storage;
use Yeltrik\ImportPDAsana\app\models\SessionAsanaAttachment;

class AsanaPDScanAttachmentRemover extends Abstract_AsanaPDScanAttachment
{

    /**
     *
     */
    public function process()
    {
        foreach ($this->sessionAsanaAttachments() as $sessionAsanaAttachment) {

            $gid = $sessionAsanaAttachment->asana_gid;
            $asanaClient = Client::accessToken(env('ASANA_PERSONAL_ACCESS_TOKEN'));
            try {
                $attachment = $asanaClient->attachments->getAttachment(
                    $gid,
                    [],
                    ['opt_fields' => 'name,download_url,created_at']
                );
            } catch (\Throwable $e) {
                switch ($e->getMessage()) {
                    case "Not Found":
                    case "Forbidden":
                        $this->processRemoval($sessionAsanaAttachment);
                        break;
                    default:
                        dd([
                            'AsanaPDScanAttachmentRemover->process()',
                            'Caught exception: ' . $e->getMessage(),
                            $sessionAsanaAttachment,
                            $this->session()
                        ]);
                        break;
                }
            }
        }
    }

    /**
     * @param SessionAsanaAttachment $sessionAsanaAttachment
     */
    private function processRemoval(SessionAsanaAttachment $sessionAsanaAttachment)
    {
        $this->processRemovalOfDownloadFile($sessionAsanaAttachment);
        $this->processRemovalOfRosters();
        $sessionAsanaAttachment->delete();
    }

    /**
     * @param SessionAsanaAttachment $sessionAsanaAttachment
     */
    private function processRemovalOfDownloadFile(SessionAsanaAttachment $sessionAsanaAttachment)
    {
        if ( $sessionAsanaAttachment->download_path != NULL ) {
            Storage::disk('public')->delete($sessionAsanaAttachment->download_path);
        }
    }

    /**
     *
     */
    private function processRemovalOfRosters()
    {
        foreach($this->session()->rosters as $roster) {
            $roster->delete();
        }
    }

}
