<?php


namespace Yeltrik\ImportPDAsana\app\import;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Yeltrik\ImportPDAsana\app\models\SessionAsanaAttachment;
use Yeltrik\ImportPDAsana\app\models\SessionAsanaTask;
use Yeltrik\PdPSR\app\models\Session;

abstract class Abstract_AsanaPDScanAttachment
{

    private Session $session;

    /**
     * AsanaPDScanAttachmentImporter constructor.
     * @param Session $session
     */
    public function __construct(Session $session)
    {
        $this->session = $session;
    }

    /**
     * @return Session
     */
    protected function session()
    {
        return $this->session;
    }

    /**
     * @return Builder|Model|object|null
     */
    protected function sessionAsanaAttachments()
    {
        return SessionAsanaAttachment::query()->where('session_id', '=', $this->session()->id)->get();
    }

    /**
     * @return Builder|Model|object|null
     */
    protected function sessionAsanaTask()
    {
        return SessionAsanaTask::query()->where('session_id', '=', $this->session()->id)->first();
    }

}
