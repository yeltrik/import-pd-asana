<?php


namespace Yeltrik\ImportPDAsana\app\import;

use Yeltrik\ImportPDAsana\app\importer\RosterImporter;
use Yeltrik\ImportProfileAsana\app\importer\ProfileImporter;
use Yeltrik\PdPSR\app\models\Roster;
use Yeltrik\PdPSR\app\models\Session;
use Yeltrik\Profile\app\models\Profile;

/**
 * Class ImportableSessionRoster
 * @package Yeltrik\ImportPDAsana\app\import
 */
class ImportableSessionRoster
{

    private Session $session;
    private array $data;

    /**
     * ImportableSessionRoster constructor.
     * @param Session $session
     * @param array $data
     */
    public function __construct(Session $session, array $data)
    {
        $this->session = $session;
        $this->data = $data;
    }

    /**
     * @param $name
     * @return mixed|null
     */
    public function __get($name)
    {
        if ($this->__isset($name)) {
            return $this->data()[$name];
        } else {
            return NULL;
        }
    }

    /**
     * @param $name
     * @return mixed
     */
    public function __isset($name)
    {
        return array_key_exists($name, $this->data());
    }

    /**
     * @return bool
     */
    public function attended()
    {
        if (isset($this->data()['Attended']) && $this->data()['Attended'] === 'Attended') {
            return TRUE;
        } elseif ($this->attendedHasColumnLabeledWithAttendedWithAValue()) {
            return TRUE;
        } else {
            return FALSE;
        }
    }

    /**
     * @return false
     */
    private function attendedHasColumnLabeledWithAttendedWithAValue()
    {
        foreach ( $this->data() as $key => $value ) {
            if ( stristr($key, 'Attendance') ) {
                if ( $key !== "Attended" ) {
                    return TRUE;
                }
            }
        }
        return FALSE;
    }

    /**
     * @return array
     */
    private function data()
    {
        return $this->data;
    }

    private function decodeName(array &$data)
    {
        if (array_key_exists('Name', $this->data())) {
            $name = $this->data()['Name'];
        } elseif (array_key_exists('Attendance Name', $this->data())) {
            $name = $this->data()['Attendance Name'];
        } elseif (array_key_exists('Asana Faculty Name', $this->data())) {
            $name = $this->data()['Asana Faculty Name'];
        } else {
            $name = NULL;
        }
        if ($name != NULL) {
            if (stristr($name, ",")) {
                $this->decodeNameCommaDelimited($data, $name);
            } else if (stristr($name, " ")) {
                $this->decodeNameSpaceDelimited($data, $name);
            } else {
                $data['Nickname'] = trim($name);
                //dd("Unsupported Name as Nickname? : " . $name);
            }
        }

        $this->packSignatureData('First Name', $data);
        $this->packSignatureData('Middle Name', $data);
        $this->packSignatureData('Last Name', $data);
    }

    /**
     * @param array $data
     * @param string $name
     */
    private function decodeNameCommaDelimited(array $data, string $name)
    {
        $array = explode(",", $name);
        $first = trim($array[1]);
        $last = trim($array[0]);
        if ($first != NULL && $last != NULL) {
            $data['First Name'] = $first;
            $data['Last Name'] = $last;
        } else {
            //dd('Last, First Name: ' . $name);
        }
    }

    /**
     * @param array $data
     * @param string $name
     */
    private function decodeNameSpaceDelimited(array $data, string $name)
    {
        $array = explode(" ", $name);
        if (sizeof($array) === 2) {
            $first = trim($array[0]);
            $last = trim($array[1]);
            if ($first != NULL && $last != NULL) {
                $data['First Name'] = $first;
                $data['Last Name'] = $last;
            } else {
                //dd([
                //    '? First Last: ' . $name,
                //    $array
                //]);
            }
        } else {
            //dd([
            //    '? First Last: ' . $name,
            //    $array
            //]);
        }
    }

    /**
     * @param string $attribute
     * @param array $data
     * @param string|null $index
     */
    public function packSignatureData(string $attribute, array &$data, string $index = NULL)
    {
        if ($index === NULL) {
            $value = $this->$attribute;
        } else {
            $value = $this->$index;
        }
        if ($value != NULL) {
            $data[$attribute] = $value;
        }
    }

    /**
     * @return array
     */
    public function getSignatureData()
    {
        $data = [];

        $this->packSignatureData('Attended', $data);
        $this->packSignatureData('Email', $data);
        $this->packSignatureData('Email', $data, 'Registration Email');
        $this->packSignatureData('Email', $data, 'Attendance Email');

        $this->decodeName($data);

        return $data;
    }

    /**
     * @return Roster|null
     */
    public function import(): ?Roster
    {
        $profile = ProfileImporter::process($this->getSignatureData());
        if ($profile instanceof Profile) {
            return RosterImporter::process($this->session(), $profile, $this->attended());
        } else {
            // TODO: Record somewhere which records are being skipped
            //if ( sizeof($this->data()) > 1 ) {
            //    dd($this->data());
            //}
            return NULL;
        }
    }

    /**
     * @return Session
     */
    private function session()
    {
        return $this->session;
    }

}
