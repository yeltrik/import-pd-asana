<?php


namespace Yeltrik\ImportPDAsana\app\import;


use DateTime;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yeltrik\ImportAsana\app\import\Abstract_ImportAsana;
use Yeltrik\ImportPDAsana\app\models\SessionAsanaAttachment;
use Yeltrik\ImportPDAsana\app\models\SessionAsanaTask;
use Yeltrik\PdPSR\app\models\Session;

class AsanaPDRosterImporter extends Abstract_ImportAsana
{

    /**
     * AsanaPDRosterImporter constructor.
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        parent::__construct($request);
    }

    /**
     * @param $rows
     * @return array
     */
    private function associateCsv($rows)
    {
        $array = array_map('str_getcsv', $rows);
        array_walk($array, function (&$a) use ($array) {
            if (sizeof($array[0]) === sizeof($a)) {
                $a = array_combine($array[0], $a);
            }
        });
        array_shift($array); # remove column header
        return $array;
    }

    /**
     * @param $data
     * @return array
     */
    public function decodeCsv($data)
    {
        $bom = pack('CCC', 0xEF, 0xBB, 0xBF);
        if (strncmp($data, $bom, 3) === 0) {
            $data = substr($data, 3);
        }
        $rows = explode("\n", $data);
        return $this->associateCsv($rows);
    }

    /**
     * @param string $mode
     * @return RedirectResponse
     */
    public function process(string $mode)
    {
        switch ($mode) {
            case 'scan_new_attachment':
                $this->processScanNewAttachment();
                break;
            case 'scan_removed_attachment':
                $this->processScanRemovedAttachment();
                break;
            case 'download_attachment':
                $this->processDownloadAttachment();
                break;
            case 'scan_attachment_roster':
                $this->processScanAttachmentRoster();
                break;
            default:
                dd('Unsupported Mode: ' . $mode);
        }

        return back()
            ->with('success', 'You have scanned rosters.');
    }

    /**
     *
     */
    private function processDownloadAttachment()
    {
        $sessionAsanaAttachments = SessionAsanaAttachment::query()
            ->whereNull('download_path');

        foreach ($sessionAsanaAttachments->get() as $sessionAsanaAttachment) {
            $asanaPDAttachmentDownloadImporter = new AsanaPDAttachmentDownloadImporter($sessionAsanaAttachment);
            $asanaPDAttachmentDownloadImporter->process();
        }
    }

    /**
     *
     */
    private function processScanNewAttachment()
    {
        foreach ($this->sessionAsanaTaskQuery()->get() as $sessionAsanaTask) {
            $asanaPDScanAttachmentImporter = new AsanaPDScanAttachmentImporter($sessionAsanaTask->session);
            $asanaPDScanAttachmentImporter->process();
            $sessionAsanaTask->last_scanned_attachments = date('Y-m-d H:i:s');
            $sessionAsanaTask->save();
        }
    }

    /**
     *
     */
    private function processScanRemovedAttachment()
    {
        foreach ($this->sessionAsanaTaskQuery()->get() as $sessionAsanaTask) {
            $asanaPDScanAttachmentRemover = new AsanaPDScanAttachmentRemover($sessionAsanaTask->session);
            $asanaPDScanAttachmentRemover->process();
            $sessionAsanaTask->last_scanned_attachments = date('Y-m-d H:i:s');
            $sessionAsanaTask->save();
        }
    }

    /**
     *
     */
    private function processScanAttachmentRoster()
    {
        $sessionAsanaAttachments = SessionAsanaAttachment::query()
            ->where('name', 'like', '%.csv')
            ->orderBy('date_imported_roster', 'asc');

        foreach ($sessionAsanaAttachments->get() as $sessionAsanaAttachment) {
            if ($sessionAsanaAttachment->download_path != NULL) {
                $attachmentData = $this->decodeCsv(Storage::disk('public')->get($sessionAsanaAttachment->download_path));
                foreach ($attachmentData as $key => $data) {
                    if ($data == [NULL] ) {
                        unset($attachmentData[$key]);
                        continue;
                    }
                    if ( $sessionAsanaAttachment->session instanceof Session ) {
                        $importableSessionRoster = new ImportableSessionRoster($sessionAsanaAttachment->session, $data);
                        $importableSessionRoster->import();
                    } else {
                        dd(['Session no longer Exists', $sessionAsanaAttachment]);

                    }
                }
                $sessionAsanaAttachment->row_count = sizeof($attachmentData);
                $sessionAsanaAttachment->save();
            }
        }
    }

    /**
     * @param int $minutes
     * @return Builder
     */
    private function sessionAsanaTaskQuery(int $minutes = 30)
    {
        $dateTime = new DateTime();
        $dateTime->modify("-$minutes minutes");
        $aged = $dateTime->format('Y-m-d H:i:s');

        return SessionAsanaTask::query()
            ->orderBy('last_scanned_attachments', 'desc')
            ->orWhereNull('last_scanned_attachments')
            ->orWhere('last_scanned_attachments', '<', $aged)
            ->orderBy('last_scanned_attachments', 'asc');
    }

    /**
     * @param Request $request
     */
    public static function validate(Request $request)
    {
        $request->validate([
            'mode' => 'required',
        ]);
    }

}
