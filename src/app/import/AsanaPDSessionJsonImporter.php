<?php


namespace Yeltrik\ImportPDAsana\app\import;


use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yeltrik\ImportPDAsana\app\Abstract_AsanaPDImporter;

class AsanaPDSessionJsonImporter extends Abstract_AsanaPDImporter
{

    const FILE_PROPERTY_NAME = "asana_export_json_file";

    /**
     * AsanaPDJsonImporter constructor.
     * @param Request $request
     * @throws FileNotFoundException
     */
    public function __construct(Request $request)
    {
        parent::__construct($request, $this->initJson($request));
    }

    /**
     * @param Request $request
     * @return array
     * @throws FileNotFoundException
     */
    public function initJson(Request $request)
    {
        $propertyName = static::FILE_PROPERTY_NAME;
        $fileName = time() . '_' . $request->$propertyName->getClientOriginalName();
        $filePath = $request->file(static::FILE_PROPERTY_NAME)->storeAs('uploads', $fileName, 'public');

        return (array)json_decode(Storage::disk('public')->get($filePath), true);
    }

    /**
     * @return RedirectResponse
     */
    public function process()
    {
        $asanaPdSessionImporter = new AsanaPDSessionImporter(
            $this->request(),
            $this->json()['data'],
            isset($this->request()->import_new_sessions),
            isset($this->request()->update_existing_sessions)
        );
        $asanaPdSessionImporter->process();

        return back()
            ->with('success', 'You have successfully upload file.');
    }

    /**
     * @param Request $request
     */
    public static function validate(Request $request)
    {
        // Validate only if Option was to Upload
        if( $request->import_method == "export_json") {
            $request->validate([
                'asana_export_json_file' => 'required|mimes:txt|max:20480',
            ]);
        }
    }

}
