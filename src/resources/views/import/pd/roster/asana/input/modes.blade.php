<h3>
    Choose a Roster Import Mode:
</h3>

@include('importPDAsana::import.pd.roster.asana.input.mode.scan_new_attachment')
<br>
@include('importPDAsana::import.pd.roster.asana.input.mode.scan_removed_attachment')
<br>
@include('importPDAsana::import.pd.roster.asana.input.mode.download_attachment')
<br>
@include('importPDAsana::import.pd.roster.asana.input.mode.scan_attachment_for_roster')
