<input
    type="radio"
    id="mode_scan_attachment_roster"
    name="mode"
    value="scan_attachment_roster"
>

<label
    for="mode_scan_attachment_roster"
>
    Scan Downloaded Attachments for Rosters
</label>
