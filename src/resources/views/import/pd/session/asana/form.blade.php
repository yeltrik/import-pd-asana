<form
    method="POST"
    action="{{ action([\Yeltrik\ImportPDAsana\app\http\controllers\ImportPDSessionAsanaController::class, 'store']) }}"
    enctype="multipart/form-data"
>
    @csrf

    @include('importPDAsana::import.pd.asana.inputs.import_by_asana_pd_project_gid')
    <br>
    @include('importPDAsana::import.pd.asana.inputs.import_by_asana_export_json_file')
    <br>

    <br>
    @include('importPDAsana::import.pd.asana.inputs.import_new_sessions')
    <br>
    @include('importPDAsana::import.pd.asana.inputs.update_existing_sessions')
    <br>

    <br>
    @include('importPDAsana::import.pd.asana.inputs.import_tags')
    <br>

    <br>
    @include('importPDAsana::import.pd.asana.inputs.import_start_on_as_start_date')
    <br>
    @include('importPDAsana::import.pd.asana.inputs.import_due_on_as_end_date')
    <br>

    <br>
    @include('importPDAsana::import.pd.asana.inputs.import_university_term')
    <br>

    <br>
    @include('importPDAsana::import.pd.asana.inputs.import_button')
</form>
