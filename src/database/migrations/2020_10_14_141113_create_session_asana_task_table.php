<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSessionAsanaTaskTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('import_pd_asana')->create('session_asana_task', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('session_id')->unique();
            $table->string('asana_gid', 16)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('import_pd_asana')->dropIfExists('session_asana_task');
    }
}
