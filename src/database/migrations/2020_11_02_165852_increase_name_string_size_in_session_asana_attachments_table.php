<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class IncreaseNameStringSizeInSessionAsanaAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('import_pd_asana')->table('session_asana_attachments', function (Blueprint $table) {
            $table->string('name', 256)->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('import_pd_asana')->table('session_asana_attachments', function (Blueprint $table) {
            $table->string('name', 128)->change();
        });
    }
}
