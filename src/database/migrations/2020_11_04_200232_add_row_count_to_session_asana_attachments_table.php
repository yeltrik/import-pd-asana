<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRowCountToSessionAsanaAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('pd_psr')->table('session_asana_attachments', function (Blueprint $table) {
            $table->integer('row_count')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('pd_psr')->table('session_asana_attachments', function (Blueprint $table) {
            $table->dropColumn('row_count');
        });
    }

}
