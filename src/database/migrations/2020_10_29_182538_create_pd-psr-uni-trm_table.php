<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePdPsrUniTrmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('import_pd_asana')->create('pd-psr-uni-trm', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('session_id')->unique();
            $table->unsignedBigInteger('term_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('import_pd_asana')->dropIfExists('pd-psr-uni-trm');
    }
}
