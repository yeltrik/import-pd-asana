<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDownloadPathToSessionAsanaAttachmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('import_pd_asana')->table('session_asana_attachments', function (Blueprint $table) {
            $table->string('download_path')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('import_pd_asana')->table('session_asana_attachments', function (Blueprint $table) {
            $table->dropColumn('download_path');
        });
    }
}
